# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  5 15:31:44 2017
 
@authors: Allison and Erin
"""
 
'''
 * Python script to demonstrate adaptive thresholding using Otsu's method.
 *
 * usage: python AdaptiveThreshold.py <filename> <kernel-size>
'''
import cv2, sys
import numpy as np
    
# get filename and kernel size values from command line
filename = sys.argv[1]
k = int(sys.argv[2])
 
# read and display the original image
img = cv2.imread(filename)
cv2.namedWindow("original", cv2.WINDOW_NORMAL)
cv2.imshow("original", img)
cv2.waitKey(0)
 
# blur and grayscale before thresholding
blur = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(blur, (k, k), 0)
 
# perform adaptive thresholding
(t, maskLayer) = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY +
    cv2.THRESH_OTSU)
 
# make a mask suitable for color images
mask = cv2.merge([maskLayer, maskLayer, maskLayer])
 
cv2.namedWindow("mask", cv2.WINDOW_NORMAL)
cv2.imshow("mask", mask)
cv2.waitKey(0)
 
# use the mask to select the "interesting" part of the image
sel = cv2.bitwise_and(img, mask)
 
# create binary image
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(gray, (5, 5), 0)
(t, binary) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY)
 
# find contours
(_, contours, _) = cv2.findContours(binary, cv2.RETR_EXTERNAL,
    cv2.CHAIN_APPROX_SIMPLE)
 
# print table of contours and sizes
print("Found %d objects." % len(contours))
for (i, c) in enumerate(contours):
    print("\tSize of contour %d: %d" % (i, len(c)))
 
# draw contours over original image
cv2.drawContours(img, contours, -1, (0, 0, 255), 5)
 
# display original image with contours
cv2.namedWindow("contour", cv2.WINDOW_NORMAL)
cv2.imshow("contour", img)
cv2.waitKey(0)
 
# display the result
cv2.namedWindow("selected", cv2.WINDOW_NORMAL)
cv2.imshow("selected", sel)
cv2.waitKey(0)
 
# counts number of green pixels and displays results
image =cv2.imread(filename)
greens = image[:,:,1]
amtGreen = np.sum(greens)
print("Green pixel amount is", amtGreen)
 
# counts the total number of pixels
image = cv2.imread(filename)
total = image[:,:,:]
amtTotal = np.sum(total)
print("Total pixel amount is", amtTotal)



"""
Here we attempted to create a mask for the size marker.
Tried to use ImageJ to find the (x,y) coordinate of the center of the circle
and radius, but the numbers are decimals.
This leads to error in execution of code becuase the code is expecting 
integer values, not floats.
Fixed it by holding down shift key!
"""
# Create the basic black image 
blackmask = np.zeros(img.shape, dtype = "uint8")

#create mask for size marker
circlemask = cv2.circle(mask,(192,834),40,(255,255,255),-1 )

# Apply the mask and display the result
maskedImg = cv2.bitwise_and(img, circlemask)
cv2.namedWindow("Masked Image", cv2.WINDOW_NORMAL)
cv2.imshow("Masked Image", maskedImg)
cv2.waitKey(0)


"""
Here, we attempted to find the white pixel count.
This section of code still needs work.
"""
#find number of white pixels in image
from PIL import Image
im = Image.open(filename)
white = 0
for pixel in im.getdata():
    if pixel == (235,237,236):
        white += 1
print('White pixel count is ',white)
        
