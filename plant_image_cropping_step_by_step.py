#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 30 11:56:53 2017

@author: diva
"""

import cv2, sys
from matplotlib import pyplot as plt

# get filename, kernel size, and threshold value from command line
filename = sys.argv[1]
k = int(sys.argv[2])
t = int(sys.argv[3])

"""
This first section of code was taken from the program ColorHistogram.py
This opens the image and creates a color histogram 
"""


# read original image, in full color, based on command
# line argument
img = cv2.imread(sys.argv[1])

# display the image 
cv2.namedWindow("Original Image", cv2.WINDOW_NORMAL)
cv2.imshow("Original Image", img)
cv2.waitKey(0)

# split into channels
channels = cv2.split(img)

# list to select colors of each channel line
colors = ("b", "g", "r") 

# create the histogram plot, with three lines, one for
# each color
plt.xlim([0, 256])
for(channel, c) in zip(channels, colors):
    histogram = cv2.calcHist([channel], [0], None, [256], [0, 256])
    plt.plot(histogram, color = c)

plt.xlabel("Color value")
plt.ylabel("Pixels")

plt.show()


"""
This section of the code is where we create our blur
This was taken from the program GaussBlur.py
"""


# apply Gaussian blur, creating a new image
blurred = cv2.GaussianBlur(img, (k, k), 0)
blurred = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# display blurred image
cv2.namedWindow("blurred", cv2.WINDOW_NORMAL)
cv2.imshow("blurred", blurred)
cv2.waitKey(0)

"""
This section of code was taken from the program Threshold.py
"""

# perform inverse binary thresholding 
(t, maskLayer) = cv2.threshold(blurred, t, 200, cv2.THRESH_BINARY)

# make a mask suitable for color images
mask = cv2.merge([maskLayer, maskLayer, maskLayer])

# display the mask image
cv2.namedWindow("mask", cv2.WINDOW_NORMAL)
cv2.imshow("mask", mask)
cv2.waitKey(0)

# use the mask to select the "interesting" part of the image
sel = cv2.bitwise_and(img, mask)

# display the result
cv2.namedWindow("selected", cv2.WINDOW_NORMAL)
cv2.imshow("selected", sel)
cv2.waitKey(0)


"""
Split channels by color
Obtain green channel
"""

Newimg = cv2.imread(filename)
b,g,r = cv2.split(Newimg)

green = cv2.imwrite('green_channel.jpg',g)

cv2.namedWindow("green",cv2.WINDOW_NORMAL)
cv2.imshow("green", green)
cv2.waitKey(0)


"""
This section of code was taken from ImgSlice.py
"""

# extract, display, and save sub-image
clip = sel[:, :, 1]
cv2.namedWindow("clip", cv2.WINDOW_NORMAL)
cv2.imshow("clip", clip)
cv2.imwrite("clip.tif", clip)
cv2.waitKey(0)


"""
try np.zero function or merge function
"""
